import java.util.Scanner;
public class Statistics
{
	public static void main(String[] args)
	{
		double num1, num2, num3, num4, num5;
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Enter the first number: ");
		num1 = keyboard.nextDouble();
		System.out.print("Enter the second number: ");
		num2 = keyboard.nextDouble();
		System.out.print("Enter the third number: ");
		num3 = keyboard.nextDouble();
		System.out.print("Enter the fourth number: ");
		num4 = keyboard.nextDouble();
		System.out.print("Enter the fifth number: ");
		num5 = keyboard.nextDouble();
		System.out.println("");

		//Sort the numbers
		double swap = 0;
		while (num1 > num2 | num2 > num3 | num3 > num4 | num4 > num5)
		{
			if(num1 > num2) {
				swap = num2;
				num2 = num1;
				num1 = swap;
			}
			if(num2 > num3) {
				swap = num3;
				num3 = num2;
				num2 = swap;
			}
			if(num3 > num4) {
				swap = num4;
				num4 = num3;
				num3 = swap;
			}
			if(num4 > num5) {
				swap = num5;
				num5 = num4;
				num4 = swap;
			}
		}
		
		//Find the mode
		int modeAmt = 1;
		double modeFind = num1;
		if(num1 == num2) {
			modeAmt++;
		}
		if(num2 == num3) {
			modeFind = num2;
			modeAmt++;
		}
		if(num3 == num4) {
			if(modeAmt == 1 | modeFind == num3) {  //Condition will fail if num1==num2 and num2!=num3
				modeAmt++;
				modeFind = num3;
			}
		}
		if(num4 == num5) {
			if(modeAmt == 1) { //Test if last two numbers is only mode
				modeAmt++;
				modeFind = num4;
			} else if(num3 == num4 && num4 == num5) {
				modeFind = num3;
				modeAmt++;
			}
		}


		System.out.println("The min value is: "+num1);
		System.out.println("The max value is: "+num5);
		System.out.println("The sum of the values is: "+(num1+num2+num3+num4+num5));
		System.out.println("The average of the values is: "+((num1+num2+num3+num4+num5)/5));
		System.out.println("The median of the values is: "+num3);
		System.out.print("The mode of the values is: ");
		if(modeAmt == 1) {
			System.out.println("None!");
		} else {
			System.out.println(modeFind);
		}
		
	}
}